package com.jo;


import static org.lwjgl.opengl.GL11.*;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.BufferedImageUtil;
import org.newdawn.slick.util.ResourceLoader;

public class Main {

	private long lastFrame;
	private int fps;
	private long lastFPS;

	private Texture texture;
	private TrueTypeFont font;
	private float y;
	private float x;
	private float rotation;
	private float size;

	private long getTime()	{
		return (Sys.getTime() * 1000 / Sys.getTimerResolution());
	}


	private int getDelta() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		return delta;
	}

	public Main () {
		try {
			Display.setDisplayMode(new DisplayMode(800,600));
			Display.setTitle("Game!");
			Display.create();
		} catch (LWJGLException e){
			e.printStackTrace();
		}

		Font awtFont = new Font("Courier New", Font.PLAIN, 11);
		font = new TrueTypeFont(awtFont, false);


		// OpenGL Initialization 
			glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 800, 600, 0, 1, -1);
		glMatrixMode(GL_MODELVIEW);

		x = 150f;
		y = 150f;


		lastFrame = getTime();


		while (!Display.isCloseRequested()) {
			//Render
			render();	
		}
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Main();

	}

	boolean mgrab = false;
	private double dx;
	private double dy;
	private double thrust = 0;

	private void render(){
		glClear(GL_COLOR_BUFFER_BIT);

		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			Display.destroy();
			System.exit(0);
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			x = Display.getWidth()/2;
			y = Display.getHeight()/2;
			thrust = 0;
		}

		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) rotation -=2.6;
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) rotation +=2.6;
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) thrust += 0.0002;


		//rotation += Mouse.getDX();
		size = 50;

		dx = Math.cos(Math.toRadians(rotation));
		dy = Math.sin(Math.toRadians(rotation));

		thrust -= 0.00008; //drag
		if(thrust <= 0) thrust = 0;

		x += dx * getDelta()/16 * thrust;
		y += dy * getDelta()/16 * thrust;

		//x = (float)Mouse.getX();
		//y = (float)480-Mouse.getY();

		if(Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
			mgrab = !mgrab;
			Mouse.setGrabbed(mgrab);
		}

		//font.drawString(20,20, " " + fps);	


		//glColor3f(1f/x,1f/y,1f/x);

		glPushMatrix();
			glTranslatef(x, y, 0);
			glRotatef(rotation-90, 0f, 0f, 1f);
			glTranslatef(-x-size/2, -y-size/2, 0);

			glBegin(GL_LINE_LOOP);
				glVertex2f(x,y);
				glVertex2f(x+size,y);
				glVertex2f(x+size,y+size);
				glVertex2f(x+size/2,y+(float)(size*1.50));
				glVertex2f(x,y+size);
			glEnd();
		glPopMatrix();

		Display.update();
		Display.sync(60);
	}

}