package com.jo;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.util.glu.*;

import java.awt.Color;
import java.awt.Font;
import java.util.Random;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

public class SideScroller {
	
	private long lastFrame;
	private int fps;
	private long lastFPS;
	
	private Texture texture;
	private TrueTypeFont font;
	private float y;
	private float x;
	private float rotation;
	private float size;
	
	private long getTime()	{
		return (Sys.getTime() * 1000 / Sys.getTimerResolution());
	}
	
	
	private int getDelta() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		return delta;
	}
	
	public SideScroller () {
		try {
			Display.setDisplayMode(new DisplayMode(1024,768));
			//Display.setFullscreen(true);
			Display.setTitle("Side Scroller Test");
			Display.setVSyncEnabled(true);
			Display.create();
		} catch (LWJGLException e){
			e.printStackTrace();
		}
		
		Font awtFont = new Font("Courier New", Font.PLAIN, 11);
		font = new TrueTypeFont(awtFont, false);
	 
		System.out.println("OpenGL Version: " + glGetString(GL_VERSION));
		// OpenGL Initialization 

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, 1024, 786, 0, -1, 1);
		glMatrixMode(GL_MODELVIEW);
		
		
		x = 0.0f;
		y = 0.0f;
		
		lastFrame = getTime();		
		
		while (!Display.isCloseRequested()) {
			//Render
			render();	
		}
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new SideScroller();

	}
	
	boolean mgrab = false;
	private float translate_x;
	
	private void render(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			

		glTranslatef(translate_x, 0f, 0f);
		
		
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) translate_x = 2;
		else if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) translate_x = -2;
		else translate_x = 0;

		if(Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
			mgrab = !mgrab;
			Mouse.setGrabbed(mgrab);
		}
		
		//font.drawString(20,20, " " + fps);
	
		glBegin(GL_LINE_LOOP);
			glVertex2f(100,100);
			glVertex2f(100+100, 100);
			glVertex2f(100+100, 100+100);
			glVertex2f(100, 100+100);
		glEnd();
		
		glBegin(GL_LINE_LOOP);
			glVertex2f(300,300);
			glVertex2f(300+300, 300);
			glVertex2f(300+300, 300+300);
			glVertex2f(300, 300+300);
		glEnd();
		
		
		Display.update();
		Display.sync(60);
		
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			Display.destroy();
			System.exit(0);
		}
	}
	

}
