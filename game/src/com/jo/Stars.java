package com.jo;

import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.util.glu.*;

import java.awt.Color;
import java.awt.Font;
import java.util.Random;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.Texture;

public class Stars {
	
	private long lastFrame;
	private int fps;
	private long lastFPS;
	
	private Texture texture;
	private TrueTypeFont font;
	private float y;
	private float x;
	private float rotation;
	private float size;
	
	Point[] points = new Point[1000];
	Random random = new Random();
	private int displayListHandle;

	
	private long getTime()	{
		return (Sys.getTime() * 1000 / Sys.getTimerResolution());
	}
	
	
	private int getDelta() {
		long currentTime = getTime();
		int delta = (int) (currentTime - lastFrame);
		return delta;
	}
	
	public Stars () {
		try {
			Display.setDisplayMode(new DisplayMode(1024,768));
			//Display.setFullscreen(true);
			Display.setTitle("Game!");
			Display.create();
		} catch (LWJGLException e){
			e.printStackTrace();
		}
		
		Font awtFont = new Font("Courier New", Font.PLAIN, 11);
		font = new TrueTypeFont(awtFont, false);
	 
		
		// OpenGL Initialization 

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective((float)30, 1024f / 768f, 0.001f, 500);
		glMatrixMode(GL_MODELVIEW);
		

		for (int i = 0; i < points.length; i++) {
			points[i] = new Point((random.nextFloat() - 0.5f) * 100f, 
								  (random.nextFloat() - 0.5f) * 100f,
								  (float)(random.nextInt(200) - 200),
								  new Color(random.nextFloat(), random.nextFloat(),random.nextFloat()));
		}
		
		speed = 0.0f;
		x = 0.0f;
		y = 0.0f;
		
		
		lastFrame = getTime();
		
		displayListHandle = glGenLists(1);
		glNewList(displayListHandle, GL_COMPILE);
			for (Point p : points) {
				glColor3b((byte)p.color.getRed(), (byte)p.color.getGreen(),(byte)p.color.getBlue());
				glBegin(GL_LINE_LOOP);
					glVertex3f(p.x, p.y, p.z);
					glVertex3f(p.x+2, p.y, p.z);
					glVertex3f(p.x+2, p.y+2, p.z);
					glVertex3f(p.x, p.y+2, p.z);
				glEnd();
				glBegin(GL_LINE_LOOP);
					glVertex3f(p.x, p.y, p.z);
					glVertex3f(p.x+2, p.y, p.z);
					glVertex3f(p.x+2, p.y, p.z-2);
					glVertex3f(p.x, p.y, p.z-2);
				glEnd();
				glBegin(GL_LINE_LOOP);
					glVertex3f(p.x, p.y, p.z);
					glVertex3f(p.x, p.y, p.z-2);
					glVertex3f(p.x, p.y+2, p.z-2);
					glVertex3f(p.x, p.y+2, p.z);
				glEnd();
				glBegin(GL_LINE_LOOP);
					glVertex3f(p.x+2, p.y, p.z);
					glVertex3f(p.x+2, p.y, p.z-2);
					glVertex3f(p.x+2, p.y+2, p.z-2);
					glVertex3f(p.x+2, p.y+2, p.z);
				glEnd();
				glBegin(GL_LINE_LOOP);
					glVertex3f(p.x, p.y, p.z-2);
					glVertex3f(p.x+2, p.y, p.z-2);
					glVertex3f(p.x+2, p.y+2, p.z-2);
					glVertex3f(p.x, p.y+2, p.z-2);	
				glEnd();
			}
		glEndList();
		
		
		while (!Display.isCloseRequested()) {
			//Render
			render();	
		}
		
		glDeleteLists(displayListHandle, 1);
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Stars();

	}
	
	boolean mgrab = false;
	private float dx;
	private float dy;
	private double thrust = 0;
	private float speed;
	private float rotx;
	private float dir;
	
	private void render(){
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
			Display.destroy();
			System.exit(0);
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)){
			x = Display.getWidth()/2;
			y = Display.getHeight()/2;
			thrust = 0;
		}
		
		dx = (float)Math.cos(Math.toRadians(rotation));
		dy = (float)Math.sin(Math.toRadians(rotation));
		
		//Move the screen
		glTranslatef(0f, 0f, speed);
		glRotatef(rotation, 0f, -rotation*dir, 1f);
		
		if(Keyboard.isKeyDown(Keyboard.KEY_DOWN)) speed -= 0.01f;
		if(Keyboard.isKeyDown(Keyboard.KEY_UP)) speed += 0.01f;
		if(Keyboard.isKeyDown(Keyboard.KEY_RIGHT)) {
			rotation += 0.007f;
			dir = -1;
		}
		if(Keyboard.isKeyDown(Keyboard.KEY_LEFT)) {
			rotation -= 0.007f;
			dir = 1;
		}

		if(speed < 0){
			speed += 0.001f; //drag
		} else if (speed > 0) {
			speed -= 0.001f; //drag
		} else
			speed = 0;
		
		if(rotation < 0){
			rotation += 0.001f; //drag
		} else if (rotation > 0) {
			rotation -= 0.001f; //drag
		} else
			rotation = 0;

		//rotation += Mouse.getDX();
		size = 50;
		
		x += dx * getDelta()/16 * thrust;
		y += dy * getDelta()/16 * thrust;
		
		//x = (float)Mouse.getX();
		//y = (float)480-Mouse.getY();
		
		if(Keyboard.isKeyDown(Keyboard.KEY_TAB)) {
			mgrab = !mgrab;
			Mouse.setGrabbed(mgrab);
		}
		
		//font.drawString(20,20, " " + fps);		
		
		glCallList(displayListHandle);
		
		
		
		/*glBegin(GL_POINTS);
		for (Point p : points) {	
			//glColor3b((byte)p.color.getRed(), (byte)p.color.getGreen(),(byte)p.color.getBlue());
			glVertex3f(p.x, p.y, p.z);
			glVertex3f(p.x, p.y, p.z);
			glVertex3f(p.x, p.y, p.z);
			glVertex3f(p.x, p.y, p.z);				
		}
	glEnd();*/
		
		Display.update();
		Display.sync(60);
	}
	
	private class Point {
		float x, y, z;
		Color color;
		
		public Point(float x, float y, float z, Color color) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.color = color;
		}
	}

}
